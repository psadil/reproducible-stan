#!/usr/bin/env bash

## This just makes sure everything is re-run even if parameters file
## hasn't been updated
#touch data/cfs_afc_ss1_wFB_Q_noCheck.dat

## Run in a detached Docker instance
JOB=$(docker run -v `pwd`:/example -u rstudio -d psadil/rstan /bin/bash -c "cd example && make pdf")

## Write job ID out to a file in case we want to keep track of a long-running job
echo $JOB > dockerjob

## Follow the logged output from the Docker instance
docker logs -f $JOB
