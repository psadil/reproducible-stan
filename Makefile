## Phony target to make pdf
.PHONY: pdf
pdf : output/results.pdf

## First, extract parameters from the text-based makefile into an R
## list saved into an RDS object file. This just makes it easier to access
## parameters by name during model execution and post-processing
output/data.csv : R/munge/loadData.R  data/cfs_afc_ss1_wFB_Q_noCheck_study.dat  data/cfs_afc_ss1_wFB_Q_noCheck.dat
	@echo --- Extracting parameters from CSV ---
	@mkdir -p $(@D)
	./$< -o $@ -ps $(word 2, $^) -pt $(word 3, $^)

## compile stan models, save stanmodel
stan/%.rda : R/compile_model.R stan/%.stan
	@echo --- Compiling Stan model ---
	@mkdir -p $(@D)
	./$< -o $@ -m $(word 2, $^)

## sample from stan models, save stanfit
output/stan_samples/%.Rds : R/run_model.R stan/%.rda output/data.csv
	@echo --- Running Stan model ---
	@mkdir -p $(@D)
	./$< -o $@ -m $(word 2, $^) -d $(word 3, $^) -c 4 -i 2500

## Run retrodictive checks
output/retro_afc_noK.csv : R/retro_afc_noK.R output/stan_samples/afc_noK.Rds output/data.csv output/stan_dat/afc_noK.Rds
	@echo --- Running Retrodictive Check, noK ---
	@mkdir -p $(@D)
	./$< -o $@ -s $(word 2, $^) -d $(word 3, $^) -r $(word 4, $^)

## Generate figures using the posterior distributions of model parameters.
output/figures/% : R/make_figures/%.R output/stan_samples/%.Rds
	@echo --- Generating figures ---
	@mkdir -p $(@D)
	@rm -rf $@
	./$< -o $@ -s $(word 2, $^)
#
# ## Generate figures using the posterior distributions of model parameters.
# output/figures/p_*.pdf : R/make_figures/st.R output/stan_samples/afc.Rds output/stan_samples/name.Rds output/stan_samples/afc_noK.Rds output/stan_samples/afc_pas.Rds output/stan_samples/linked.Rds output/data.csv output/retro_afc_noK.csv
# 	@echo --- Generating figures ---.
# 	@mkdir -p $(@D)
# 	./$< -o output/figures -a $(word 2, $^) -n $(word 3, $^) -k $(word 4, $^) -p $(word 5, $^) -l $(word 6, $^) -d $(word 7, $^) -s $(word 8, $^)

## Translate from Rmarkdown to markdown using knitr
output/results.md : presentations/results.Rmd output/figures/p_*.pdf
	@echo ----Translating results from RMD to Markdown----
	@mkdir -p $(@D)
	Rscript \
		-e "require(knitr)" \
                -e "knitr::render_markdown()"\
		-e "knitr::knit('$<','$@')"

## Generate PDF from resulting markdown
output/results.pdf : output/results.md
	@echo ----Generating PDF from Markdown----
	@mkdir -p $(@D)
	pandoc $< -V geometry:margin=1.0in --from=markdown -t latex -s -o $@  -V fontsize=12pt

.PHONY: clean
clean :
	@echo --- Removing generated files ---
	rm -rf output
	rm output/*.rda
