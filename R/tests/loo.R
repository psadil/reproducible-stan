#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   retro_linked.R [-m <stan_samples_1> -n <stan_samples_2>]

Options:
   -m RDS file with samples, no criterion [default: output/stan_samples/linked_reduced3]
   -n RDS file with samples, no criterion [default: output/stan_samples/linked_reduced2]
 ]' -> doc

opts <- docopt(doc)

# reduced3 -> only condition effects
# reduced2 -> condition + participant effects

fit_1 <- readRDS(paste0(opts$m, '.rds'))
fit_2 <- readRDS(paste0(opts$n, '.rds'))


suppressMessages(require(loo))
suppressMessages(require(rstan))


log_lik_1 <- loo::extract_log_lik(fit_1)
loo_1 <- loo::loo(log_lik_1)

log_lik_2 <- loo::extract_log_lik(fit_2)
loo_2 <- loo::loo(log_lik_2)

loo_diff <- loo::compare(loo_1, loo_2)

print(loo_diff)

print(loo_1)


waic_1 <- loo::waic(log_lik_1)
print(waic_1)
