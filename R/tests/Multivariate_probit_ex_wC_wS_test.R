#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
runmodel.R [-d<simmed_data> -m <model> -o <output> -r <stan_dat> -c <chains> -i <iterations> -w <warmup>]

Options:
  -d CSV file with samples [default: output/stan_samples/linked_wC_wS_simple_split_sim_d]
  -m Stan model file, naming [default: stan/linked_wC_wS_simple_split_sim]
  -o Rds Output file [default: output/stan_samples/linked_wC_wS_simple_split_sim]
  -r stan_dat file [default: output/stan_dat/linked_wC_wS_simple_split_sim]
  -c Number of chains [default: 4]
  -i Number of MCMC iterations [default: 1250]
  -w Number of MCMC iterations for warmup [default: 750]
]' -> doc

opts <- docopt(doc)

dir.name <- file.path('output', 'stan_dat')

if (!dir.exists(dir.name)){
    dir.create(dir.name)
}

suppressMessages(require(rstan))
suppressMessages(require(rstudioapi))
suppressMessages(require(rio))
suppressMessages(require(dplyr))

# Load in Stan for estimation purposes
options(mc.cores = parallel::detectCores()) # To run in parallel

stan_dat <- as.list(rio::import(paste0(opts$r, '.rds')))

seed_value = 2831 # For reproducibility

fit <- rstan::sampling(object = readRDS(paste0(opts$m, '.rda')),
                       iter = as.numeric(opts$i) + as.numeric(opts$w),
                       data = stan_dat,
                       warmup = as.numeric(opts$w),
                       chains = as.numeric(opts$c),
                       pars = c('z',
                                'z_neg', 'z_pos',
                                'zeta', 'rho_raw_i',
                                'Sigma'), include = FALSE
                       , seed = seed_value
                       # , control = list(adapt_delta = 0.9) #, max_treedepth = 15)
                       
)
# save stan_samples
dir.name <- file.path('output', 'stan_samples') 
if (!dir.exists(dir.name)){
    dir.create(dir.name)
}
saveRDS(fit, paste0(opts$o, '.rds'))


#shinystan::launch_shinystan(fit)

