#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   retro_linked.R [-m <stan_samples_1> -n <stan_samples_2>]

Options:
   -m RDS file with samples, no criterion [default: output/stan_samples/linked_reduced3]
   -n RDS file with samples, no criterion [default: output/stan_samples/linked_reduced3_kFold]
 ]' -> doc

opts <- docopt(doc)

suppressMessages(require(loo))
suppressMessages(require(rstan))
# library(rstanarm)

options(mc.cores = parallel::detectCores())

# fit_1 <- readRDS(paste0(opts$m,'.rds'))


fit_1 <- readRDS(paste0(opts$n, as.character(1),'.rds'))

log_lik_1 <- loo::extract_log_lik(fit_1, parameter_name = "log_lik_t")

loo_1 <- loo::loo(log_lik_1)

rm(fit_1)


fit_2 <- readRDS(paste0(opts$n, as.character(2),'.rds'))

log_lik_2 <- loo::extract_log_lik(fit_2)
loo_2 <- loo::loo(log_lik_2)
rm(fit_2)


loo_diff <- loo::compare(loo_1, loo_2)



fit_3 <- readRDS(paste0(opts$n, as.character(3),'.rds'))
fit_4 <- readRDS(paste0(opts$n, as.character(4),'.rds'))
fit_5 <- readRDS(paste0(opts$n, as.character(5),'.rds'))
fit_6 <- readRDS(paste0(opts$n, as.character(6),'.rds'))
fit_7 <- readRDS(paste0(opts$n, as.character(7),'.rds'))
fit_8 <- readRDS(paste0(opts$n, as.character(8),'.rds'))
fit_9 <- readRDS(paste0(opts$n, as.character(9),'.rds'))
fit_10 <- readRDS(paste0(opts$n, as.character(10),'.rds'))





print(loo_diff)

print(loo_1)


waic_1 <- loo::waic(log_lik_1)
print(waic_1)
