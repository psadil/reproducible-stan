#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   retro_linked.R [-d <data_type> -p <pattern> ]

Options:
   -d data type [default: sim]
   -p pattern [default: Multivariate_probit_ex_v3_ps_sim]
 ]' -> doc

opts <- docopt(doc)

source(file.path(getwd(), 'R', 'dirs.R'))

# Purpose:
# Computes posterior predictions (at the group-level) for a multivariate 
# probit model with group and subject-level effects.
# Arguments:
# X_small   - A K by U matrix giving the smallest set of unique conditions to 
#             predict
# Beta      - An array giving the S by 2 by K posterior samples for the 
#             group-level coefficients, where S is the number of posterior samples
# rho_mu    - A vector of length S, the posterior samples for the group-level 
#             average of the correlation coefficient
# rho_sigma - A vector of length S, the posterior samples for the group-level 
#             standard deviation of the correlation coefficient
# tau       - A S by 2 matrix, the posterior samples for the standard deviations 
#             of the independent subject-level effects
# Nt        - A vector of length K, the number of trials per each unique condition U
# Returns:
# A 4 by U by S array giving the simulated frequencies of each response type 
# (0|0, 0|1, 1|0, and 1|1, respectively) across unique conditions and posterior 
# samples.
function_script = '
arma::cube group_pred( 
arma::mat X_small,
arma::cube Beta,
arma::vec rho_mu,
arma::vec rho_sigma,
arma::mat tau,
Rcpp::IntegerVector Nt ) {

// Variable declarations
int S = Beta.n_rows; // Number of posterior samples
int K = X_small.n_rows; // Number of covariates
int U = X_small.n_cols; // Number of unique trial types to predict
double rho; // Correlation
arma::mat eta(2,1); // Subject-level effects
arma::mat Beta_small(2,K); // Coefficient matrix
arma::mat Mu(2,1); // Bivariate means
arma::mat Sigma(2,2); // Covariance matrix
arma::mat v(2,1); // Random deviates from normal
arma::mat z(1,2); // Latent variables underlying binary choice
arma::cube freq(4,U,S); // Frequencies of response types

// Fixed values
Sigma.ones(); // Fill with ones
freq.zeros(); // Fill with zeros

// Loop over posterior samples
for ( int s = 0; s < S; s++ ) {

// Draw a sample from a truncated normal for the correlation
rho = -2.0;
while ( (rho < -1.0) | (rho > 1.0) ) {
rho = R::rnorm( rho_mu(s), rho_sigma(s) );
}

// Loop over conditions
for (int u = 0; u < U; u++) {
// Loop over trials
for (int n = 0; n < Nt(u); n++) {

// Simulate subject-level effects
eta(0,0) = R::rnorm( 0.0, tau(s,0) );
eta(1,0) = R::rnorm( 0.0, tau(s,1) );

// Extract group-level effects
Beta_small = Beta.subcube(s,0,0,s,1,K-1);

// Calculate the bivariate means
Mu = Beta_small * X_small.submat(0,u,K-1,u) + eta;
// Determine the covariance matrix
Sigma(0,1) = rho; Sigma(1,0) = rho;

// Simulate values from a bivariate normal
v = arma::randn(2, 1); // Independent normal deviates
z = Mu + arma::chol(Sigma)*v; // Simulate latent variables

if (z(0) < 0.0) {
if (z(1) < 0.0) {
freq(0,u,s) = freq(0,u,s) + 1.0;
} else {
freq(1,u,s) = freq(1,u,s) + 1.0;
}
} else {
if (z(1) < 0.0) {
freq(2,u,s) = freq(2,u,s) + 1.0;
} else {
freq(3,u,s) = freq(3,u,s) + 1.0;
}
}

}

}
}

return freq;
}
'

# Compile the function
cppFunction(function_script,depends="RcppArmadillo")
rm( function_script )
