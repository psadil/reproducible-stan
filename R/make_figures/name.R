#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   name.R [-s <samples> -o <outdir>]

Options:
   -s RDS file with samples, no criterion [default: output/stan_samples/name.Rds]
   -o Output directory [default: output/figures/name]
 ]' -> doc

opts <- docopt(doc)

# make the output directory, if it doesn't already exist
if (!dir.exists(opts$o)) {
    dir.create(opts$o)
}

suppressMessages(require(ggplot2))
suppressMessages(require(Hmisc))
suppressMessages(require(rstan))
suppressMessages(require(gridExtra))
suppressMessages(require(dplyr))
suppressMessages(require(tidyr))
suppressMessages(require(readr))


fit <- readRDS(opts$s)
post = rstan::extract(fit)

post$beta<-post$beta+as.vector(post$b0) # note, to plot by condition, need to add intercept
post$b<-as.matrix(cbind(post$b0,post$beta))

df <- as.data.frame(post)[,c('b0','beta.1','beta.2','beta.3')] %>%
    tidyr::gather(., key=cond, value = beta_recall_z, b0:beta.3) %>%
    mutate(., cond = plyr::mapvalues(cond, from=c('b0','beta.1','beta.2','beta.3'), to=c('not studied','word','cfs','binocular')) ) %>%
    mutate(., beta_recall_raw = beta_recall_z) %>%
    mutate(., beta_recall=pnorm(beta_recall_z)) %>%
    mutate(., cond=factor(cond, levels=c('not studied','word','cfs','binocular')))

ggplot( df, aes( x = cond, y = beta_recall_z, fill = cond ) ) +
    geom_violin() + labs( list( x = 'Condition', y = 'Posterior Values (log scale)' ) ) +
    guides(fill=FALSE) +
    scale_y_continuous(breaks=seq(from=-1, to=3, by=1)) +
    theme(        
        text = element_text(size=25), 
        axis.ticks.x = element_line(size=1), 
        axis.ticks.length=unit(.25,"cm"), 
        legend.position=c(.75, .75),
        legend.key.height=unit(2,"line"), 
        legend.key = element_rect(fill=NA, colour=NA),
        plot.margin=unit(c(0,0,.25,0),"cm")) +
    ggsave(filename = file.path(opts$o, 'name_beta_z.png'), width = 10, height = 10)
