#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   retro_linked.R [-d <data_type> -p <pattern> ]

Options:
   -d data type [default: sim]
   -p pattern [default: Multivariate_probit_ex_v3_ps_sim]
 ]' -> doc

opts <- docopt(doc)

source(file.path(getwd(), 'R', 'dirs.R'))

dir.out <- file.path(dir.figures, opts$p)
if (!dir.exists(dir.out)){
    dir.create(dir.out)
}

if(opts$d == 'sim'){
    dir.d <- file.path(dir.sim, opts$p)
}else if(opts$d == 'data'){
    dir.d <- file.path(dir.data, 'updated')
}

suppressMessages(require(ggplot2))
suppressMessages(require(Hmisc))
suppressMessages(require(gridExtra))
suppressMessages(require(dplyr))
suppressMessages(require(tidyr))
suppressMessages(require(rio))
suppressMessages(require(ggmcmc))
suppressMessages(require(tibble))

fit <- readRDS(file.path(dir.samples, paste0(opts$p, '.rds')))

# rho <- rstan::extract(fit, pars = c('rho'), permuted = FALSE)
# r <- data.frame(rho) %>%
#     gather(param, value, chain.1.rho.1.:chain.4.rho.4.) %>%
#     as_tibble() %>%
#     separate(., col = param, into = c('ch', 'chain', 'param', 'condition')) %>%
#     select(-ch) %>%
#     mutate(condition = )

# y_rep <- rstan::extract(fit, pars = c('y_rep'), permuted = FALSE)
# rm(fit)
# y <- data.frame(y_rep) %>%
#     gather(param, resp, everything()) %>%
#     separate(col = param, into = c('ch', 'chain', 'param1', 'param2', 'obs', 'Q')) %>%
#     select(-ch, -param1, -param2, -chain) %>%
#     as_tibble() 
# rm(y_rep)

stan_dat <- as.list(rio::import(paste0(opts$r, '.rds')))

S<-ggmcmc::ggs(fit, family = "y_rep")


y_rep <- S %>%
    tidyr::extract(Parameter, c("Parameter", "obs", "Q"),
                   "([[:alnum:]]+)\\[([[:digit:]]+),([[:digit:]])") %>%
    dplyr::mutate(obs = as.numeric(obs)) %>%
    dplyr::mutate(condition = stan_dat$index_cond[obs]) %>%
    dplyr::mutate(condition = plyr::mapvalues(condition,
                                              from = c(1,2,3,4),
                                              to = c('not studied', 'word', 'cfs', 'binocular'))) %>%
    dplyr::mutate(condition = factor(condition, levels = c('not studied', 'word', 'cfs', 'binocular'))) %>%
    dplyr::mutate(Q = plyr::mapvalues(Q, from = c(1,2), to = c('afc','named'))) %>%
    dplyr::mutate(Q = factor(Q, levels = c('afc','named')))
rm(S)

tilde <- y_rep %>%
    spread(Q, value) %>%
    dplyr::mutate(joint = ifelse(!afc & !named, '{0,0}',
                          ifelse(afc & !named, '{1,0}',
                                 ifelse(!afc & named, '{0,1}', '{1,1}')))) %>%
    dplyr::mutate(joint = factor(joint, levels = c('{0,0}','{1,0}','{0,1}','{1,1}'))) %>%
    mutate(subject = stan_dat$index_s[obs]) %>%
    mutate(whichItem = stan_dat$index_i[obs])

# d <- rio::import(paste0(opts$d, '.csv'), setclass = 'tbl_df') %>%
#     dplyr::mutate(condition = plyr::mapvalues(index_cond,
#                                               from = c(1,2,3,4),
#                                               to = c('not studied', 'word', 'cfs', 'binocular'))) %>%
#     dplyr::mutate(condition = factor(condition, levels = c('not studied', 'word', 'cfs', 'binocular'))) %>%
#     dplyr::mutate(joint = ifelse(!afc & !name, '{0,0}',
#                                  ifelse(afc & !name, '{1,0}',
#                                         ifelse(!afc & name, '{0,1}', '{1,1}')))) %>%
#     dplyr::mutate(joint = factor(joint, levels = c('{0,0}','{1,0}','{0,1}','{1,1}'))) %>%
#     dplyr::mutate(Obs = 1:n())  


d <- rio::import(paste0(opts$d, '.csv'), setclass = 'tbl_df') %>%
    dplyr::mutate(., condition=factor(condition,levels=c('not studied','word','cfs','binocular'))) %>%
    dplyr::mutate(joint = ifelse(!afc & !named, '{0,0}',
                          ifelse(afc & !named, '{1,0}',
                                 ifelse(!afc & named, '{0,1}', '{1,1}')))) %>%
    dplyr::mutate(joint = factor(joint, levels = c('{0,0}','{1,0}','{0,1}','{1,1}'))) %>%
    dplyr::mutate(Obs = 1:n()) %>%
    mutate(Iteration = rep(1, tiles = n()))


d2  <- d %>% 
    group_by(Iteration, condition, joint) %>%
    summarise(avg = n() / nrow(d)) %>%
    mutate(type = rep('data', n())) 


tilde2 <- tilde %>% 
    group_by(Iteration, condition, joint) %>%
    summarise(avg = n() / (nrow(d)*4 )) %>%
    mutate(type = rep('sim', n()))
final <- rbind(d2,tilde2)


# following should be 1
tilde2 %>%
    group_by(Iteration) %>%
    summarise(ss <- sum(avg))

# following should be 1
# sum( d2$avg)

ggplot(final) + 
    facet_wrap(~condition) +
    stat_summary(mapping = aes(x=joint, y=avg, color = type),
                 fun.data = "median_hilow", fun.args = list(conf.int = .95), size = .5) +
    stat_summary(mapping = aes(x=joint, y=avg, color = type),
                 fun.data = "median_hilow", fun.args = list(conf.int = .5), size = 1) +
    scale_y_continuous(limits=c(0, .25), breaks = seq(from=0, to=.25, by=0.25),
                       name="proportion in each cell") +
    scale_x_discrete(name = "Joint cell (Black => data, Blue => Model)") +
    theme(        
        text = element_text(size=25), 
        axis.ticks.x = element_line(size=1), 
        axis.ticks.length=unit(.25,"cm"), 
        plot.margin=unit(c(0,0,.25,0),"cm")) +
    ggsave(filename = file.path(opts$o, "retro.png"), width = 10, height = 10)


d2  <- d %>%
    group_by(condition, subject) %>%
    mutate(ni = n_distinct(whichItem)) %>%
    ungroup() %>%
    group_by(Iteration, condition, joint, subject) %>%
    summarise(avg = n() / mean(ni)) %>%
    mutate(type = rep('data', times = n()))
    


tilde2  <- tilde %>%
    mutate(whichItem = stan_dat$index_i[obs])  %>%
    group_by(condition, subject) %>%
    mutate(ni = n_distinct(whichItem)*max(Chain)) %>%
    ungroup() %>%
    group_by(Iteration, condition, joint, subject) %>%
    summarise(avg = mean(ni) / (sum(ni)*4)) %>%
    mutate(type = rep('sim', times = n()))
        
final <- rbind(d2, tilde2)

filter(final, subject %in% 1:5) %>%
    ggplot(.) + 
    facet_wrap(~condition*subject) +
    stat_summary(mapping = aes(x=joint, y=avg, color = type),
                 fun.data = "median_hilow", fun.args = list(conf.int = .95), size = .5) +
    stat_summary(mapping = aes(x=joint, y=avg, color = type),
                 fun.data = "median_hilow", fun.args = list(conf.int = .5), size = 1) +
    scale_y_continuous(limits=c(0, .25), breaks = seq(from=0, to=.25, by=0.25),
                       name="proportion in each cell") +
    scale_x_discrete(name = "Joint cell (Black => data, Blue => Model)") +
    theme(        
        text = element_text(size=25), 
        axis.ticks.x = element_line(size=1), 
        axis.ticks.length=unit(.25,"cm"), 
        plot.margin=unit(c(0,0,.25,0),"cm")) +
    ggsave(filename = file.path(opts$o, "retro_sub1-5.png"), width = 10, height = 10)
