#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   linked_constrained.R [-d <data> -s <samples> -r <stan_dat> -o <outdir>]

Options:
   -d CSV file with samples [default: output/data.csv]
   -s RDS file with samples, no criterion [default: output/stan_samples/linked_constrained_lkj.Rds]
   -r RDS file with stan_data [default: output/stan_dat/linked_constrained_lkj.Rds] 
   -o Output file [default: output/linked_constrained_lkj.csv]
 ]' -> doc

opts <- docopt(doc)

suppressMessages(require(tidyr))

dir.name <- file.path('output', 'retro') 

if (!dir.exists(dir.name)){
    dir.create(dir.name)
}

## Load samples
fit <- readRDS(opts$s)

post <- rstan::extract(fit)
post$beta <- NULL
post$eta <- NULL
post$lp__ <- NULL
post$sigma <- NULL

S<-ggmcmc::ggs(fit, family = "beta\\[.+,.\\]")
rm(fit)


betas <- S %>%
    filter(Chain == 1) %>%
    select(-Chain) %>%
    tidyr::extract(Parameter, c("Parameter", "Q", "condition"), "([[:alnum:]]+)\\[([[:digit:]]),([[:digit:]])") %>%
    dplyr::mutate(., Q = plyr::mapvalues(Q, from = c(1,2), to = c('afc','named'))) %>%
    dplyr::mutate(., Q = factor(Q, levels = c('afc','named'))) 
rm(S)


# load original data
stan_dat <- readRDS(opts$r)


# Generate random subject and item level effects per posterior sample
# and then calculate the probability of picking right, generate a set of data
# collapse over conditions
pred_ac <- tibble::tibble(Iteration = rep(1:2500, times = 8), 
                          question = rep(c('afc','named'), each = 10000),
                          condition = rep(1:4, each = 2500, times = 2),
                          acc = rep(NA, times = 20000)
)


tilde <- tibble::tibble(Obs = rep(1:stan_dat$No, times = 2), 
                        question = rep(c('afc','named'), each = stan_dat$No),
                        acc = rep(NA, times = stan_dat$No * 2),
                        joint = rep(NA, times = stan_dat$No * 2)
                        
) %>%
    mutate(condition = stan_dat$index_cond[Obs])
tilde <- tilde[rep(1:(stan_dat$No*2),times = 250),] %>% 
    mutate(Iteration = rep(1:250, each = stan_dat$No * 2))
    



# Create a progress bar
pb = txtProgressBar( min = 1, max = 250, style = 3 )
for ( i in 1:250 ) {
    zeta <- matrix(rnorm( stan_dat$Ni * 2,  0, post$sigma_i[i] ), nrow = 2)
    eta <- matrix(rnorm( stan_dat$Ns * 2, 0, post$sigma_s[i] ), nrow = 2)

    tmp <- as.matrix(betas %>% filter(Iteration == i & Q == 'afc') %>% select(value)) 
    mu1 <- stan_dat$x %*% tmp + eta[1, stan_dat$index_s] + zeta[1, stan_dat$index_i]

    tmp <- as.matrix(betas %>% filter(Iteration == i & Q == 'named') %>% select(value)) 
    mu2 <- stan_dat$x %*% tmp + eta[2, stan_dat$index_s] + zeta[2, stan_dat$index_i]
    
    mu <- cbind(mu1,mu2)
    
    z <- matrix(0, nrow = stan_dat$No, ncol = 2)
    for (o in 1:stan_dat$No){
        z[o,] <- MASS::mvrnorm(1, mu = mu[o,], Sigma = post$Omega[i,o,,])
    }
    Y <- z
    Y[z<0] <- 0
    Y[z>0] <- 1
    
    tilde <- tilde %>%
        mutate(acc = replace(acc, Iteration == i & question == 'afc', Y[,1]))
    
    tilde <- tilde %>%
        mutate(acc = replace(acc, Iteration == i & question == 'named', Y[,2]))
    
    # Update the progress bar
    setTxtProgressBar(pb,i)
}
close(pb)


tilde <- tilde %>%
    dplyr::mutate(condition = plyr::mapvalues(condition, from = c(1,2,3,4), to = c('not studied', 'word', 'cfs', 'binocular')))

readr::write_csv(tilde, opts$o)
