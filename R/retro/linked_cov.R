#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
linked2.R [-d <data> -s <samples> -r <stan_dat> -o <outdir>]

Options:
-d CSV file with samples [default: output/data.csv]
-s RDS file with samples, no criterion [default: output/stan_samples/linked_cov.Rds]
-r RDS file with stan_data [default: output/stan_dat/linked_cov.Rds] 
-o Output file [default: output/linked_cov.csv]
]' -> doc

opts <- docopt(doc)

suppressMessages(require(tidyr))
library(dplyr)

dir.name <- file.path('output', 'retro') 

if (!dir.exists(dir.name)){
    dir.create(dir.name)
}

## Load samples
fit <- readRDS(opts$s)

post <- rstan::extract(fit)
rm(fit)


# load original data
stan_dat <- readRDS(opts$r)



# Generate random subject and item level effects per posterior sample
# and then calculate the probability of picking right, generate a set of data
# collapse over conditions
pred_ac <- tibble::tibble(Iteration = rep(1:2500, times = 8), 
                          question = rep(c('afc','named'), each = 10000),
                          condition = rep(1:4, each = 2500, times = 2),
                          acc = rep(NA, times = 20000)
)


tilde <- tibble::tibble(Obs = rep(1:stan_dat$No, times = 2), 
                        question = rep(c('afc','named'), each = stan_dat$No),
                        acc = rep(NA, times = stan_dat$No * 2),
                        joint = rep(NA, times = stan_dat$No * 2)
                        
) %>%
    mutate(condition = stan_dat$index_cond[Obs])
tilde <- tilde[rep(1:(stan_dat$No*2),times = 250),] %>% 
    mutate(Iteration = rep(1:250, each = stan_dat$No * 2))




# Create a progress bar
mu <- cbind(0,0)
pb = txtProgressBar( min = 1, max = 250, style = 3 )
for ( i in 1:500 ) {

    z <- matrix(0, nrow = stan_dat$No, ncol = 2)
    for (o in 1:stan_dat$No){
        Sigma_grand = (diag(x=post$sigma[i, , stan_dat$index_cond[o]], nrow=2) %*%
                           post$Omega[i, stan_dat$index_cond[o], , ] %*%
                           diag(x=post$sigma[i, , stan_dat$index_cond[o]], nrow=2)) + 
            (diag(x=post$sigma_i[i, , stan_dat$index_i[o]], nrow=2) %*%
                 post$Omega_i[i, stan_dat$index_i[o], , ] %*% 
                 diag(x=post$sigma_i[i, , stan_dat$index_i[o]], nrow=2)) +
            (diag(x=post$sigma_s[i, , stan_dat$index_s[o]], nrow=2) %*%
                 post$Omega_s[i, stan_dat$index_s[o], , ] %*% 
                 diag(x=post$sigma_s[i, , stan_dat$index_s[o]], nrow=2))
        
        z[o,] <- MASS::mvrnorm(1, mu = mu, Sigma = Sigma_grand)
    }
    Y <- z
    Y[z<0] <- 0
    Y[z>0] <- 1
    
    tilde <- tilde %>%
        mutate(acc = replace(acc, Iteration == i & question == 'afc', Y[,1]))
    
    tilde <- tilde %>%
        mutate(acc = replace(acc, Iteration == i & question == 'named', Y[,2]))
    
    # Update the progress bar
    setTxtProgressBar(pb,i)
}
close(pb)


tilde <- tilde %>%
    dplyr::mutate(condition = plyr::mapvalues(condition, from = c(1,2,3,4), to = c('not studied', 'word', 'cfs', 'binocular')))

readr::write_csv(tilde, opts$o)
