#! /usr/bin/Rscript
suppressMessages(require(docopt))

'Usage:
   loadData.R [-ps <data> -pt <study data> -o <output> ]

Options:
   -ps .dat file with study data [default: data/cfs_afc_ss1_wFB_Q_noCheck_study_updated.dat]
   -pt .dat file with test data [default: data/cfs_afc_ss1_wFB_Q_noCheck_updated.dat]
 ]' -> doc

opts <- docopt(doc)

suppressMessages(require(readr))
suppressMessages(require(dplyr))


# Load study data
data_study <- readr::read_csv(opts$ps)

# Load test data
data_test <- readr::read_csv(opts$pt)

# join by subject, and item
d <- inner_join(x=data_test,y=data_study, by=c('whichItem','subject')) %>%
    rename(., condition=condition.x) %>%
    mutate(., pas1 = plyr::mapvalues(studyResp, from=0:9, to=c(-1,3,2,1,0,0,1,2,3,NA))) %>%
    mutate(., pas2 = plyr::mapvalues(studyResp2, from=0:9, to=c(-1,3,2,1,0,0,1,2,3,NA))) %>%
    mutate(., condition = plyr::mapvalues(condition, from=1:4, to=c('not studied','word','cfs','binocular'))) %>%
    filter(., !is.na(whichResp) & whichResp != 2 & !is.nan(pas1) & !whichResp==-1) 


itemsPre = unique(d$whichItem)
itemsPost = 1:128

d <- mutate(d, whichItem = plyr::mapvalues(whichItem, from=itemsPre, to=itemsPost)) %>%
    mutate(., pas1 = plyr::mapvalues(pas1, from=c(-1, NA), c('NR','fNS'))) %>%
    mutate(., pas2 = plyr::mapvalues(pas2, from=c(-1, NA), c('NR','fNS'))) %>%
    mutate(., condition=factor(condition,levels=c('not studied','word','cfs','binocular')))

readr::write_csv(d, 'output/data_updated.csv')

# d %>% group_by(condition) %>% summarise(n = n())

# d2 <- d %>%
#     filter(!(condition == 'cfs' & pas2 > 2)) %>%
#     filter(!(condition == 'binocular' & pas2 < 2)) %>%
#     filter(!(condition == 'word' & pas2 < 2)) %>%
#     group_by(condition, subject) %>%
#     mutate(ni = n()) %>%
#     ungroup() %>%
#     filter( ni > 10) %>%
#     group_by(subject) %>%
#     mutate(nConds = length(unique(condition))) %>%
#     ungroup() %>%
#     filter( nConds < 4 )
# 
# subsPre = unique(d2$subject)
# subsPost = 1:length(unique(d2$subject))
# 
# d2 <- mutate(d2, subject = plyr::mapvalues(subject, from=subsPre, to=subsPost))

readr::write_csv(d2, 'output/data_updated_trim.csv')

# d2 %>% group_by(condition) %>% summarise(n = n())