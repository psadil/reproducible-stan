#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   runmodel.R [-m <model> -o <output> -c <chains> -i <iterations>]

Options:
   -m Stan model file, naming [default: stan/linked2.rda]
   -o Rds Output file [default: output/stan_samples/linked2.Rds]
   -c Number of chains [default: 4]
   -i Number of MCMC iterations [default: 2500]
 ]' -> doc

opts <- docopt(doc)

###Sim multilevel dataset####

K <- 4 #number of betas (Condition)
D <- 2 # number of decisions
Ns <- 30 #number of subjects
Ni <- 128 #number of items
No <- Ni*Ns # number of observations

# design matrix
ones <- rep(1,times=Ni/K)
zeros <- rep(0,times=Ni/K)
x <- cbind(c(ones,rep(zeros,times=3)),
           c(zeros,ones,rep(zeros,times=2)),
           c(rep(zeros,times=2),ones,zeros),
           c(rep(zeros,times=3),ones)
)
X <- do.call("rbind", rep(list(x), Ns))
index_cond <- which(X==1, arr.ind=TRUE)[,2]
index_s <- rep(1:Ns,each=Ni);
index_i <- rep(sample(x=1:Ni,size=Ni),Ns);


# variances
sigma <- rcauchy(D,location=0,scale=2.5)
sigma_i <- rcauchy(D,location=0,scale=2.5)
sigma_s <- rcauchy(D,location=0,scale=2.5)

# correlation matrix
o1 <- cbind(c(1,0.1),c(0.1,1))
o2 <- cbind(c(1,0.9),c(0.9,1))
Omega <- array(0,dim=c(D,D,K))
Omega[,,1] <- o1
Omega[,,2] <- o2
Omega[,,3] <- o1
Omega[,,4] <- o2

Omega_s <- o1 # subject
Omega_i <- o1 # item

# Cholesky factorization
L_Omega <- array(0,dim=dim(Omega))
for (k in 1:K){
    L_Omega[,,k] <- t(chol(Omega[,,k]))
}
L_Omega_s <- t(chol(Omega_s))
L_Omega_i <- t(chol(Omega_i))

# Lambdas
Lam <- array(0,dim=dim(Omega))
for (k in 1:K){
    Lam[,,k] <- L_Omega[,,k] %*% diag(sigma)
}
Lam_s <- L_Omega_s %*% diag(sigma_s)
Lam_i <- L_Omega_i %*% diag(sigma_i)

s <- cbind(c(1,0),c(0,1))
mu <- c(0,0)

beta <- matrix(MASS::mvrnorm(n=K, c(.5,.3), s),nrow=D)
eta <- matrix(MASS::mvrnorm(n=Ns, mu, s),nrow=D)
zeta <- matrix(MASS::mvrnorm(n=Ni, mu, s),nrow=D)

for (k in 1:K){
    beta[,k] <- beta[,k] %*% Lam[,,k]
}
for (i in 1:Ni){
    zeta[,i] <- zeta[,i] %*% Lam_i
}
for (s in 1:Ns){
    eta[,s] <- eta[,s] %*% Lam_s
}

# s <- cbind(c(1,0),c(0,1))
# for (k in 1:K){
#     beta[,k] <- matrix(MASS::mvrnorm(n=K, mu, s),nrow=D)
# }
# s <- cbind(c(1,0),c(0,1))
# for (i in 1:Ni){
#     zeta <- matrix(MASS::mvrnorm(n=Ns, mu, s),nrow=D)
# }
# for (s in 1:Ns){
#     eta <- matrix(MASS::mvrnorm(n=Ns, mu, s),nrow=D)
# }


theta <- matrix(0, No, D);
for (d in 1:D){
    for (o in 1:No){
        theta[o,d] <- pnorm(X[o,]%*%beta[d,]  + eta[d,index_s[o]] + zeta[d,index_i[o]])
    }
}

y <- matrix(0, No, D);
#correct <- matrix(0, No, D);
for (d in 1:D){
    y[,d] <- rbinom(No, 1, theta[,d])
#    correct[,d] <- rbinom(No, 1, theta[,d])
}

stan_dat <- list(
    No = No, # number of observations
    Ns = Ns, # number subjects
    Ni = Ni, # number of items
    D = D, # number of responses (2afc, named)
    K = K, # The number of covariates for d' (conditions)
    x = X,
    index_s = index_s,
    index_i = index_i,
    index_cond = index_cond,
#    Correct = correct[,1],
    y = y # Subject's choice (0 = left, 1 = right); length(Y) == No
)    

library(rstan)
options(mc.cores = parallel::detectCores())
burn <- 500 # Burn-in
fit <- rstan::sampling(object = readRDS(opts$m),
                       iter = as.numeric(opts$i) + burn,
                       data = stan_dat,
                       warmup = burn,
                       chains = as.numeric(opts$c),
                       pars = c('beta','Omega'), include=TRUE
                       #chains = 1
)

saveRDS(fit, opts$o)

#shinystan::launch_shinystan(fit)

## Load samples
fit <- readRDS(opts$o)
post <- rstan::extract(fit)


library(ggmcmc)
S<-ggs(fit)

dplyr::filter(S, Parameter %in% c('beta[1,1]',
                                  'beta[1,2]',
                                  'beta[1,3]',
                                  'beta[1,4]',
                                  'beta[2,1]',
                                  'beta[2,2]',
                                  'beta[2,3]',
                                  'beta[2,4]')) %>%
    ggs_pairs(.)
