data {
  int<lower=1> Ns; // Number of subjects
  int<lower=1> No; // Number of trials per subject
  int<lower=1> K; // Number of covariates
  int<lower=1> D; // number of outcomes (2)
  int<lower=1> N_pos; // Total number of successes
  int<lower=1> N_neg; // Total number of failures
  int I_pos[N_pos, 2]; // Indices for row, column, and subject for successes
  int I_neg[N_neg, 2]; // Indices for row, column, and subject for failures
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct

}
parameters {
  // The Stan manual (see pg. 81 - 84) shows how a multivariate
  // probit model can be estimated using a trick proposed by
  // Albert and Chib (1993). To employ this data augmentation
  // technique, we have passed into Stan a set of matrices
  // giving the row, column, and subject index for an array
  // of latent variables associated with successes or failures.
  // The model assumes that if the value of a latent
  // variable is positive, a success occurs, otherwise
  // a failure occurs. The technique from Albert and Chib
  // capitalizes on this fact by defining a latent variable
  // z that is truncated to be only positive for successes
  // and only negative for failures.

  matrix[D, K] beta; // Group-level coefficients for the bivariate normal means
  vector[D] eta[Ns]; // Subject-level coefficients for the bivariate normal means
  vector<lower=0.0>[D] tau; // Variance of subject-level effects
  vector<lower=-1.0,upper=1.0>[No] rho; // Subject-level correlation coefficients
  vector[Ns] s_rho_mu; // subject influence on correlation
  vector[K] cond_rho_mu; // condition influence on correlation
  vector<lower=0.0>[K] rho_sigma; // Group-level standard deviation for correlation
  vector<lower=0>[N_pos] z_pos; // The positive values of z
  vector<upper=0>[N_neg] z_neg; // The negative values of z
}
transformed parameters {
  // We reconstruct the latent variable z for each subject
  // from its positive and negative components.
  vector[D] Mu[No]; // Bivariate normal means per trial
  vector[D] z[No];
  vector[No] rho_mu; // mean for correlation on each trial


  for (n in 1:N_pos)
    z[I_pos[n,1], I_pos[n,2] ] = z_pos[n];
  for (n in 1:N_neg)
    z[I_neg[n,1], I_neg[n,2] ] = z_neg[n];

    for (o in 1:No) {
      Mu[o] = beta*x[o] + eta[index_s[o]];
      rho_mu[o] = cond_rho_mu[index_cond[o]] + s_rho_mu[index_s[o]];
    }
}
model {
  matrix[2,2] Sigma; // Covariance matrix for latent variable Z

  // Priors
  to_vector(beta) ~ normal(0.0,5.0); // Mildly informative priors on other parameters
  s_rho_mu ~ normal(0.0,.5);
  cond_rho_mu ~ normal(0.0,.25);
  rho_sigma ~ gamma(2.0,4.0);
  tau ~ gamma(2.0,4.0);

  // Fill in fixed values
  Sigma[1,1] = 1.0;  Sigma[2,2] = 1.0;

  // Hierarchy
  for (s in 1:Ns) {
    eta[s, 1] ~ normal(0.0, tau[1] );
    eta[s, 2] ~ normal(0.0, tau[2] );
  }

  // Likelihood
  {
    for (o in 1:No) {
      rho[o] ~ normal(rho_mu[o], rho_sigma[index_cond[o]] ) T[-1.0,1.0];

      Sigma[1,2] = rho[o]; Sigma[2,1] = rho[o];

      // Note that the likelihood is based on the sampled values
      // of the latent variable (i.e. z_pos and z_neg)
      z[o] ~ multi_normal(Mu[o], Sigma);
    }
  }
}
