data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  cholesky_factor_corr[D] L;
  matrix[ K+Ns, 2 ] priors; // includes rho
}
parameters{
}
model {

}
generated quantities{
  matrix[D, K] beta; // condition effects on mean of latent
  matrix[Ns, D] eta; // Subject effects on mean of latent

  vector[D] Mu[No];
  vector[D] z[No];

{
  vector[D] m;
  matrix[D, D] Sigma;
  m = rep_vector(0, D);

  Sigma[1,1] = 1.0; Sigma[2,2] = 1.0;

  for(k in 1:K){
    Sigma[1,2] = priors[k, 1]; Sigma[2,1] = priors[k, 1];
    beta[, k] = multi_normal_rng(m, Sigma);
  }
  for(s in 1:Ns){
    Sigma[1,2] = priors[K+s, 1]; Sigma[2,1] = priors[K+s, 1];
    eta[s] = to_row_vector(multi_normal_rng(m, Sigma));
  }
}

for(o in 1:No){
  Mu[o] = beta*x[o] + to_vector(eta[index_s[o]]);
  z[o] = multi_normal_cholesky_rng(Mu[o], L);
}

}
