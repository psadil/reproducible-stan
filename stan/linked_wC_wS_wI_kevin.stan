data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y[No, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
  int<lower=0> N_pos; // The number of successes (1)
  int<lower=0> N_neg; // The number of failures (0)
}
transformed data {
  // The Stan manual (see pg. 81 - 84) shows how a multivariate
  // probit model can be estimated using a trick proposed by
  // Albert and Chib (1993). To employ this data augmentation
  // technique, we first define a set of indices for the row
  // and columns associated with successes (pos) or failures
  // (neg):
  int<lower=1, upper=No> row_pos[N_pos];
  int<lower=1, upper=No> col_pos[N_pos];
  int<lower=1, upper=No> row_neg[N_neg];
  int<lower=1, upper=No> col_neg[N_neg];

  // Create a local block
  {
    int inc_pos;
    int inc_neg;

    inc_pos = 1;
    inc_neg = 1;

    for (o in 1:No) { // Loop over trials

      for (i in 1:2) { // Loop over columns
        // If observed data is a success, store
        // index for positive values
        if (y[o, i] == 1) {
          row_pos[inc_pos] = o;
          col_pos[inc_pos] = i;
          inc_pos = inc_pos + 1;
        } else {
          // If observed data is a failure, store
          // index for negative values
          row_neg[inc_neg] = o;
          col_neg[inc_neg] = i;
          inc_neg = inc_neg + 1;
        }
      }
    }

  }
}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  vector[Ns] eta[D]; // Subject effects on mean of latent
  vector[Ni] zeta[D]; // Subject effects on mean of latent
  vector<lower=-1.0, upper=1.0>[K] rho; // Correlation coefficient
  vector<lower=-1.0, upper=1.0>[Ns] rho_raw_s; // Correlation coefficient
  vector<lower=-1.0, upper=1.0>[Ni] rho_raw_i; // Correlation coefficient
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  vector<lower=-1.0, upper=1.0>[No] rho_raw; // Correlation coefficient
}
transformed parameters {
  // We reconstruct the latent variable z from its
  // positive and negative components.

  vector[D] z[No];
  cov_matrix[D] Sigma[No];

  for (n in 1:N_pos)
    z[row_pos[n], col_pos[n]] = z_pos[n];
  for (n in 1:N_neg)
    z[row_neg[n], col_neg[n]] = z_neg[n];

  {
    for(o in 1:No){
      Sigma[o][1, 1] = 1.0; Sigma[o][2, 1] = rho_raw[o];
      Sigma[o][1, 2] = rho_raw[o]; Sigma[o][2, 2] = 1.0;
    }
  }
}
model {

  // priors
  rho ~ normal(0.0, .25);
  rho_raw_s ~ normal(0.0, .25);
  rho_raw_i ~ normal(0.0, .25);

  {
    vector[No] mu_rho;

    for(o in 1:No)
      mu_rho[o] = rho[index_cond[o]] + rho_raw_s[index_s[o]] + rho_raw_i[index_i[o]];

   rho_raw ~ normal(mu_rho, 0.25);
  }

  for (d in 1:D){
    beta[d] ~ normal(0, 5);
    eta[d] ~ normal(0, 5);
    zeta[d] ~ normal(0, 5);
  }

  // likelihood
  for (o in 1:No){
    z[o] ~ multi_normal(beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_s[o]]), Sigma[o]);
  }

}

generated quantities {
  // vector[K] rho; // correlation matrix to assemble
  int<lower=0,upper=1> y_rep[No, D]; // this is the posterior retroodictive check
  vector[No] log_lik; // log-likelihood, for calculating loo/waic

  // {
  //   matrix[D, D] Omega;
  //   for (k in 1:K){
  //     Omega = multiply_lower_tri_self_transpose(L_Omega[k]);
  //     rho[k] = Omega[1,2];
  //   }
  // }

  {
    vector[D] z_rep;
    for (o in 1:No) {
      log_lik[o] = multi_normal_lpdf(z[o] | beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_s[o]]), Sigma[o]);
      z_rep = multi_normal_rng(beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_s[o]]), Sigma[o]);
      for (d in 1:D) {
        if (z_rep[d] <= 0)
        y_rep[o, d] = 0;
        else
        y_rep[o, d] = 1;
      }
    }
  }

}
