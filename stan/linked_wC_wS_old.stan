functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y[No, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
  cholesky_factor_corr[D] L;
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)];
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)];
  int<lower=1,upper=D> d_neg[size(n_neg)];

  N_pos = size(n_pos);
  N_neg = size(n_neg);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) {
          n_pos[i] = n;
          d_pos[i] = d;
          i = i + 1;
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }

}
parameters{
  vector[D] beta[K]; // condition effects on mean of latent
  // vector[D] eta[Ns]; // Subject effects on mean of latent
  cholesky_factor_corr[D] L_Omega[K];
  // cholesky_factor_corr[D] L_Omega_s[Ns]; // Variance for subject-level effects
  vector<lower=0>[K] eta_lkj; // condition effects on mean of latent
  // vector<lower=0>[Ns] eta_lkj_s; // condition effects on mean of latent
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
}
transformed parameters {
  vector[D] z[No];

  for (n in 1:N_pos)
    z[n_pos[n], d_pos[n]] = z_pos[n];
  for (n in 1:N_neg)
    z[n_neg[n], d_neg[n]] = z_neg[n];
}
model {
  vector[D] mu;

  mu = rep_vector(0.0, D);

  z_pos ~ normal(0, 1);
  z_neg ~ normal(0, 1);

  // priors
  eta_lkj ~ gamma(priors[1, 1], priors[1, 2]);

  for (k in 1:K) {
    L_Omega[k] ~ lkj_corr_cholesky(eta_lkj[k]);
    beta[k] ~ multi_normal_cholesky(mu, L_Omega[k]);
  }

  // eta_lkj_s ~ gamma(priors[2, 1], priors[2, 2]);
  // for(s in 1:Ns){
  //   L_Omega_s[s] ~ lkj_corr_cholesky(eta_lkj_s[s]);
  //   eta[s] ~ multi_normal_cholesky(mu, L_Omega_s[s]);
  // }

  // likelihood
  {
    vector[D] beta_x[No];
    for (o in 1:No)
      beta_x[o] = beta[index_cond[o]];// + eta[index_s[o]];
    z ~ multi_normal_cholesky(beta_x, L);
  }
}

generated quantities {
  vector[K] rho; // correlation matrix to assemble
  int<lower=0,upper=1> y_rep[No, D]; // this is the posterior retroodictive check
  vector[No] log_lik; // log-likelihood, for calculating loo/waic

  {
    matrix[D, D] Omega;
    for (k in 1:K){
      Omega = multiply_lower_tri_self_transpose(L_Omega[k]);
      rho[k] = Omega[1,2];
    }
  }

  {
    vector[D] z_rep;

    for (o in 1:No){
      log_lik[o] = multi_normal_cholesky_lpdf(z | beta[index_cond[o]], L);
      z_rep = multi_normal_cholesky_rng(beta[index_cond[o]], L);
      for (d in 1:D){
        if (z_rep[d] <= 0)
          y_rep[o, d] = 0;
        else
          y_rep[o, d] = 1;
      }
    }
  }
}
