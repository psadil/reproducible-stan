data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0,upper=1> y[No, D];
}

parameters{
  vector[D] z[No];
  corr_matrix[D] Omega[K]; // Variance for subject-level effects
  corr_matrix[D] Omega_s[Ns]; // Variance for subject-level effects
  corr_matrix[D] Omega_i[Ni]; // Variance for item-level effects
  matrix<lower=0>[D, K] sigma;
  matrix<lower=0>[D, Ni] sigma_i;
  matrix<lower=0>[D, Ns] sigma_s;
}

transformed parameters{
    matrix<lower = 0, upper = 1>[No, D] theta;
    
    for(d in 1:D){
        for(o in 1:No){
          theta[o, d] = Phi_approx(z[o, d]);    
        }
        
    }
}

model {
  vector[D] mu;
  mu = rep_vector(0.0, D);
  
  // priors on correlations
  for (k in 1:K){
    Omega[k] ~ lkj_corr(2.0);    
  }
  for (i in 1:Ni){
    Omega_i[i] ~ lkj_corr(2.0);
  }
  for (s in 1:Ns){
    Omega_i[s] ~ lkj_corr(2.0);
  }
  
  // priors on scale
  for (d in 1:D){
    sigma[d] ~ gamma(2,4);
    sigma_s[d] ~ gamma(2,4);
    sigma_i[d] ~ gamma(2,4);
  }

  // linear equation
  {
      matrix[D, D] Sigma_grand;
      for (o in 1:No){
          Sigma_grand = quad_form_diag(Omega[index_cond[o]], sigma[,index_cond[o]]) + quad_form_diag(Omega_s[index_s[o]], sigma_s[,index_s[o]]) + quad_form_diag(Omega_i[index_i[o]], sigma_i[,index_i[o]]);
          // Sigma_grand = Omega[index_cond[o]] + Omega_s[index_s[o]] + Omega_i[index_i[o]];
          z[o] ~ multi_normal(mu, Sigma_grand);
      }
  }
  
  // likelihood
  for(d in 1:D){
          y[, d] ~ bernoulli(theta[, d]);
      }
}
