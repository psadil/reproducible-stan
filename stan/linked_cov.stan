functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0,upper=1> y[No, D];
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)];
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)];
  int<lower=1,upper=D> d_neg[size(n_neg)];
  N_pos = size(n_pos);
  N_neg = size(n_neg);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) {
          n_pos[i] = n;
          d_pos[i] = d;
          i = i + 1;
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }
}
parameters{
  matrix[D, K] beta;
  matrix[Ni, D] zeta; // Subject-level effects for d' and kappa
  matrix[Ns, D] eta; // Subject-level effects for d' and kappa
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  corr_matrix[D] Omega[K]; // Variance for subject-level effects
  corr_matrix[D] Omega_s[Ns]; // Variance for subject-level effects
  corr_matrix[D] Omega_i[Ni]; // Variance for item-level effects
  matrix<lower=0>[D, K] sigma;
  matrix<lower=0>[D, Ni] sigma_i;
  matrix<lower=0>[D, Ns] sigma_s;
}
transformed parameters {
  vector[D] z[No];
  cholesky_factor_corr[D] Omega_grand_L[No];

  
  for (n in 1:N_pos) {
    z[n_pos[n], d_pos[n]] = z_pos[n];
  }
  for (n in 1:N_neg) {
    z[n_neg[n], d_neg[n]] = z_neg[n];
  }

    {
      matrix[D, D] Sigma_grand;
      vector[D] s_raw;
      vector[D] s_sqrt;
      matrix[D, D] S;
      matrix[D, D] Omega_grand;
      
      for (o in 1:No){
          Sigma_grand = quad_form_diag(Omega[index_cond[o]], sigma[,index_cond[o]]) +
                        quad_form_diag(Omega_s[index_s[o]], sigma_s[,index_s[o]]) +
                        quad_form_diag(Omega_i[index_i[o]], sigma_i[,index_i[o]]);
    s_raw = diagonal(Sigma_grand);
    for(i in 1:D){
      s_sqrt[i] = sqrt(s_raw[i]);
    }
    S = s_sqrt * to_row_vector(s_sqrt);
    Omega_grand = Sigma_grand ./ S;
    Omega_grand_L[o] = cholesky_decompose(Omega_grand);
      }
}


}
model {
  vector[D] mu;
  // matrix[D, D] L_Sigma;

  mu = rep_vector(0.0, D);

  
  // priors on correlations
  for (k in 1:K){
    Omega[k] ~ lkj_corr(2.0);    
  }
  for (i in 1:Ni){
    Omega_i[i] ~ lkj_corr(2.0);
  }
  for (s in 1:Ns){
    Omega_i[s] ~ lkj_corr(2.0);
  }
  
  // priors on scale
  for (d in 1:D){
    sigma[d] ~ gamma(2,4);
    sigma_s[d] ~ gamma(2,4);
    sigma_i[d] ~ gamma(2,4);
  }

  // priors on intercepts and condition effects
  // for (d in 1:D){
  //   beta[d] ~ normal(0, sigma[d]);
  //   zeta[d] ~ normal(0, sigma_i[d]);
  //   eta[d] ~ normal(0, sigma_s[d]);      
  // }
  
  // for (k in 1:K){
  //   L_Sigma = diag_pre_multiply(sigma[, k], Omega[k]);
  //   beta[,k] ~ multi_normal_cholesky(mu, L_Sigma);
  // }
  // for (s in 1:Ns){
  //   L_Sigma = diag_pre_multiply(sigma_s[, s], Omega_s[s]);
  //   eta[s] ~ multi_normal_cholesky(mu, L_Sigma);
  // }
  // for (i in 1:Ni){
  //   L_Sigma = diag_pre_multiply(sigma_i[, i], Omega_i[i]);
  //   zeta[i] ~ multi_normal_cholesky(mu, L_Sigma);
  // }
  

  // likelihood
  // NOTE: define y_i such that if (z_sim_i > 1) {y_i=1}, else {y_i=0}
  // z can be used for posterior retroodictive check
      for (o in 1:No){
          // z[o] ~ multi_normal_cholesky(beta*x[o] + eta[index_s[o]] + zeta[index_i[o]], Omega_grand_L[o]);
          z[o] ~ multi_normal_cholesky(mu, Omega_grand_L[o]);
      }
}

