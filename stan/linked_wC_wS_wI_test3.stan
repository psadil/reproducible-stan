data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y[No, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
  int<lower=0> N_pos; // The number of successes (1)
  int<lower=0> N_neg; // The number of failures (0)
}
transformed data {
  // The Stan manual (see pg. 81 - 84) shows how a multivariate
  // probit model can be estimated using a trick proposed by
  // Albert and Chib (1993). To employ this data augmentation
  // technique, we first define a set of indices for the row
  // and columns associated with successes (pos) or failures
  // (neg):
  int<lower=1, upper=No> row_pos[N_pos];
  int<lower=1, upper=No> col_pos[N_pos];
  int<lower=1, upper=No> row_neg[N_neg];
  int<lower=1, upper=No> col_neg[N_neg];

  // Create a local block
  {
    int inc_pos;
    int inc_neg;

    inc_pos = 1;
    inc_neg = 1;

    for (o in 1:No) { // Loop over trials

      for (i in 1:2) { // Loop over columns
        // If observed data is a success, store
        // index for positive values
        if (y[o, i] == 1) {
          row_pos[inc_pos] = o;
          col_pos[inc_pos] = i;
          inc_pos = inc_pos + 1;
        } else {
          // If observed data is a failure, store
          // index for negative values
          row_neg[inc_neg] = o;
          col_neg[inc_neg] = i;
          inc_neg = inc_neg + 1;
        }
      }
    }

  }
}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  vector[Ns] eta[D]; // Subject effects on mean of latent
  vector[Ni] zeta[D]; // Subject effects on mean of latent
  cholesky_factor_corr[D] L_Omega[K];
  vector<lower=0>[K] eta_lkj; // condition effects on mean of latent
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
}
transformed parameters {
  // We reconstruct the latent variable z from its
  // positive and negative components.
  vector[D] z[No];
  vector[D] Mu[No];

  for (n in 1:N_pos)
    z[row_pos[n], col_pos[n]] = z_pos[n];
  for (n in 1:N_neg)
    z[row_neg[n], col_neg[n]] = z_neg[n];

  for(o in 1:No)
    Mu[o] = beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_i[o]]);

}
model {

  // priors
  for (k in 1:K) {
    eta_lkj[k] ~ gamma(priors[1, 1], priors[1, 2]);  // implicitly truncated below at 0 to become half-cauchy
    L_Omega[k] ~ lkj_corr_cholesky(eta_lkj[k]);
  }

  for (d in 1:D){
    beta[d] ~ normal(0, 5);
    eta[d] ~ normal(0, 5);
    zeta[d] ~ normal(0, 5);
  }

  // likelihood
  for (o in 1:No)
    z[o] ~ multi_normal_cholesky(Mu[o], L_Omega[index_cond[o]]);

}

generated quantities {
  vector<lower=-1,upper=1>[K] rho; // correlation matrix to assemble
  corr_matrix[D] Omega[K];
  matrix<lower=0,upper=1>[No, D] y_rep; // this is the posterior retroodictive check

  for (k in 1:K){
    Omega[k] = multiply_lower_tri_self_transpose(L_Omega[k]);
    rho[k] = Omega[k][1,2];
  }

  {
    vector[D] z_rep;
    for (o in 1:No) {
      z_rep = multi_normal_cholesky_rng(Mu[o], L_Omega[index_cond[o]]);
      for (d in 1:D) {
        if (z_rep[d] <= 0)
          y_rep[o, d] = 0;
        else
          y_rep[o, d] = 1;
      }
    }
  }

}
