data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y[No, D];
  cholesky_factor_corr[D] L;
  matrix[ 2, 2 ] priors; //
  int<lower=0> N_pos; // The number of successes (1)
  int<lower=0> N_neg; // The number of failures (0)
  int I_pos[N_pos, 2]; // Indices for row, column
  int I_neg[N_neg, 2]; // Indices for row, column
}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  matrix[Ns, D] eta; // Subject effects on mean of latent
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  vector<lower=0>[K] cond_eta;
  vector<lower=0>[Ns] s_eta;
  vector<lower=-1, upper=1>[K] cond_rho;
  vector<lower=-1, upper=1>[Ns] s_rho;
}
transformed parameters {
  // We reconstruct the latent variable z from its
  // positive and negative components.
  vector[D] z[No];
  vector[D] Mu[No];

  for (n in 1:N_pos)
    z[I_pos[n,1], I_pos[n,2] ] = z_pos[n];
  for (n in 1:N_neg)
    z[I_neg[n,1], I_neg[n,2] ] = z_neg[n];

    for(o in 1:No)
      Mu[o] = beta*x[o] + to_vector(eta[index_s[o]]);

}
model {

  //
  cond_eta ~ gamma(priors[1, 1], priors[1, 2]);
  s_eta ~ gamma(priors[2, 1], priors[2, 2]);

{
  vector[D] m;
  matrix[D, D] Sigma;
  m = rep_vector(0, D);

  Sigma[1,1] = 1.0; Sigma[2,2] = 1.0;

  for(k in 1:K){
    cond_rho[k] ~ normal(0, cond_eta[k]) T[-1, 1];
    Sigma[1,2] = cond_rho[k]; Sigma[2,1] = cond_rho[k];
    beta[, k] ~ multi_normal(m, Sigma);
  }
  for(s in 1:Ns){
    s_rho[s] ~ normal(0, s_eta[s]) T[-1, 1];
    Sigma[1,2] = s_rho[s]; Sigma[2,1] = s_rho[s];
    eta[s] ~ multi_normal(m, Sigma);
  }
}

  // likelihood
  z ~ multi_normal_cholesky(Mu, L);

}
