data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  cholesky_factor_corr[D] L;
  matrix[ 1 + 3 + 1, 2 ] priors; //
}
model {

}
generated quantities {
  matrix[D, K] beta; // condition effects on mean of latent
  matrix[Ns, D] eta; // Subject effects on mean of latent
  vector<lower=0>[K] cond_eta;
  vector<lower=0>[Ns] s_eta;
  cholesky_factor_corr[D] cond_L_omega[K];
  cholesky_factor_corr[D] s_L_omega[Ns];

  vector[K] rho;
  vector[Ns] rho_s;
  vector[D] m;
  vector[D] Mu[No];

  vector[D] z[No];

  m = rep_vector(0, D);

  for(k in 1:K){
    cond_eta[k] = gamma_rng(priors[k, 1], priors[k, 2]);
    cond_L_omega[k] = lkj_corr_cholesky_rng(D, cond_eta[k]);
    beta[, k] = multi_normal_cholesky_rng(m, cond_L_omega[k]);
  }

  for(s in 1:Ns){
    s_eta[s] = gamma_rng(priors[K+1, 1], priors[K+1, 2]);
    s_L_omega[s] = lkj_corr_cholesky_rng(D, s_eta[s]);
    eta[s] = to_row_vector(multi_normal_cholesky_rng(m, s_L_omega[s]));
  }

  for(o in 1:No)
    Mu[o] = beta*x[o] + to_vector(eta[index_s[o]]);

  // likelihood
  for(o in 1:No)
    z[o] = multi_normal_cholesky_rng(Mu[o], L);

  {
    matrix[D, D] omega;
    for(k in 1:K){
      omega = multiply_lower_tri_self_transpose(cond_L_omega[k]);
      rho[k] = omega[1,2];
    }

    for(s in 1:Ns){
      omega = multiply_lower_tri_self_transpose(s_L_omega[s]);
      rho_s[s] = omega[1,2];
    }
  }
}
