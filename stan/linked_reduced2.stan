functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  int<lower=0,upper=1> y[No, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)];
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)];
  int<lower=1,upper=D> d_neg[size(n_neg)];
  N_pos = size(n_pos);
  N_neg = size(n_neg);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) {
          n_pos[i] = n;
          d_pos[i] = d;
          i = i + 1;
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }
}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  vector[Ns] eta[D]; // Subject effects on mean of latent
  cholesky_factor_corr[D] L_Omega[K];
  vector<lower=0>[K] eta_lkj; // condition effects on mean of latent
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  vector<lower=0.0>[D] sigma; // Variance for subject-level effects
  vector<lower=0.0>[D] sigma_s; // Variance for subject-level effects
  // real<lower=0.0> sigma_s; // Variance for subject-level effects
}
transformed parameters {
  vector[D] z[No];
  for (n in 1:N_pos) {
    z[n_pos[n], d_pos[n]] = z_pos[n];
  }
  for (n in 1:N_neg) {
    z[n_neg[n], d_neg[n]] = z_neg[n];
  }
}
model {
  // priors
  for (k in 1:K) {
    eta_lkj[k] ~ gamma(priors[1, 1], priors[1, 2]);  // implicitly truncated below at 0 to become half-cauchy
    L_Omega[k] ~ lkj_corr_cholesky(eta_lkj[k]);
  }

  sigma ~ gamma(priors[2, 1], priors[2, 2]);
  sigma_s ~ gamma(priors[3, 1], priors[3, 2]);

  for (d in 1:D){
    beta[d] ~ normal(0, sigma[d]);
    eta[d] ~ normal(0, sigma_s[d]);

    // print("target_before = ", target());
    // if (d == 1){
    //   eta[d] ~ gamma(priors[5, 1], priors[5, 2]);
    //   print("eta[1] = ", eta[1]);
    // }
    // else{
    //   eta[d] ~ normal(0, sigma_s);
    //   print("eta[2] = ", eta[2]);
    // }
    // print("target_after = ", target());
  }

  // likelihood
  for (o in 1:No){
    z[o] ~ multi_normal_cholesky(beta*x[o] + to_vector(eta[, index_s[o]]), L_Omega[index_cond[o]]);
  }

}

generated quantities {
  vector[K] rho; // correlation matrix to assemble
  int<lower=0,upper=1> y_rep[No, D]; // this is the posterior retroodictive check
  vector[No] log_lik; // log-likelihood, for calculating loo/waic

  {
    matrix[D, D] Omega;
    for (k in 1:K){
      Omega = multiply_lower_tri_self_transpose(L_Omega[k]);
      rho[k] = Omega[1,2];
    }
  }

  {
    vector[D] z_rep;
    for (o in 1:No) {
      log_lik[o] = multi_normal_cholesky_lpdf(z[o] | beta*x[o] + to_vector(eta[, index_s[o]]), L_Omega[index_cond[o]]);
      z_rep = multi_normal_cholesky_rng(beta*x[o] + to_vector(eta[, index_s[o]]), L_Omega[index_cond[o]]);
      for (d in 1:D) {
        if (z_rep[d] <= 0)
          y_rep[o, d] = 0;
        else
          y_rep[o, d] = 1;
      }
    }
  }


}
