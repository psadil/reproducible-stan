functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns_t; // The number of subjects
  int<lower=1> Ni_t; // The number of items
  int<lower=0, upper=Ns_t*Ni_t> No_t; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x_t[No_t]; // design matrix
  int<lower=1,upper=K> index_cond_t[No_t]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y_t[No_t, D];
  int<lower=1> Ns_h; // The number of subjects
  int<lower=1> Ni_h; // The number of items
  int<lower=0, upper=Ns_h*Ni_h> No_h; // number of observations
  vector<lower=0,upper=1>[K] x_h[No_h]; // design matrix
  int<lower=1,upper=K> index_cond_h[No_h]; // Index indicating which questions were gotten correct
  int<lower=0, upper=1> y_h[No_h, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
}
transformed data {
  int<lower=0> N_pos_t;
  int<lower=1,upper=No_t> n_pos_t[my_sum(y_t)];
  int<lower=1,upper=D> d_pos_t[size(n_pos_t)];
  int<lower=0> N_neg_t;
  int<lower=1,upper=No_t> n_neg_t[(No_t * D) - size(n_pos_t)];
  int<lower=1,upper=D> d_neg_t[size(n_neg_t)];

  int<lower=0> N_pos_h;
  int<lower=1,upper=No_h> n_pos_h[my_sum(y_h)];
  int<lower=1,upper=D> d_pos_h[size(n_pos_h)];
  int<lower=0> N_neg_h;
  int<lower=1,upper=No_h> n_neg_h[(No_h * D) - size(n_pos_h)];
  int<lower=1,upper=D> d_neg_h[size(n_neg_h)];

  N_pos_t = size(n_pos_t);
  N_neg_t = size(n_neg_t);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No_t) {
      for (d in 1:D) {
        if (y_t[n, d] == 1) {
          n_pos_t[i] = n;
          d_pos_t[i] = d;
          i = i + 1;
        } else {
          n_neg_t[j] = n;
          d_neg_t[j] = d;
          j = j + 1;
        }
      }
    }
  }

  N_pos_h = size(n_pos_h);
  N_neg_h = size(n_neg_h);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No_h) {
      for (d in 1:D) {
        if (y_h[n, d] == 1) {
          n_pos_h[i] = n;
          d_pos_h[i] = d;
          i = i + 1;
        } else {
          n_neg_h[j] = n;
          d_neg_h[j] = d;
          j = j + 1;
        }
      }
    }
  }

}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  cholesky_factor_corr[D] L_Omega[K];
  vector<lower=0>[K] eta_lkj; // condition effects on mean of latent
  vector<lower=0>[N_pos_t] z_pos_t;
  vector<upper=0>[N_neg_t] z_neg_t;
  // vector<lower=0>[N_pos_h] z_pos_h;
  // vector<upper=0>[N_neg_h] z_neg_h;
  vector<lower=0.0>[D] sigma; // Variance for subject-level effects
}
transformed parameters {
  vector[D] z_t[No_t];
  // vector[D] z_h[No_h];

  for (n in 1:N_pos_t) {
    z_t[n_pos_t[n], d_pos_t[n]] = z_pos_t[n];
  }
  for (n in 1:N_neg_t) {
    z_t[n_neg_t[n], d_neg_t[n]] = z_neg_t[n];
  }

  // for (n in 1:N_pos_h) {
  //   z_h[n_pos_h[n], d_pos_h[n]] = z_pos_h[n];
  // }
  // for (n in 1:N_neg_h) {
  //   z_h[n_neg_h[n], d_neg_h[n]] = z_neg_h[n];
  // }

}
model {
  // priors
  for (k in 1:K) {
    eta_lkj[k] ~ gamma(priors[1, 1], priors[1, 2]);  // implicitly truncated below at 0 to become half-cauchy
    L_Omega[k] ~ lkj_corr_cholesky(eta_lkj[k]);
  }

  sigma ~ gamma(priors[2, 1], priors[2, 2]);

  for (d in 1:D){
    beta[d] ~ normal(0, sigma[d]);

    // print("target_before = ", target());
    // if (d == 1){
    //   eta[d] ~ gamma(priors[5, 1], priors[5, 2]);
    //   print("eta[1] = ", eta[1]);
    // }
    // else{
    //   eta[d] ~ normal(0, sigma_s);
    //   print("eta[2] = ", eta[2]);
    // }
    // print("target_after = ", target());
  }

  // likelihood
  for (o in 1:No_t){
    z_t[o] ~ multi_normal_cholesky(beta*x_t[o], L_Omega[index_cond_t[o]]);
  }

}

generated quantities {
  vector[K] rho; // correlation matrix to assemble
  int<lower=0,upper=1> y_rep[No_t, D]; // this is the posterior retroodictive check
  vector[No_t] log_lik_t; // log-likelihood, for calculating loo/waic
  vector[No_h] log_lik_h; // log-likelihood, for calculating loo/waic

  {
    matrix[D, D] Omega;
    for (k in 1:K){
      Omega = multiply_lower_tri_self_transpose(L_Omega[k]);
      rho[k] = Omega[1,2];
    }
  }

  {
    vector[D] z_rep;
    for (o in 1:No_t) {
      log_lik_t[o] = multi_normal_cholesky_lpdf(z_t[o] | beta*x_t[o], L_Omega[index_cond_t[o]]);
      z_rep = multi_normal_cholesky_rng(beta*x_t[o], L_Omega[index_cond_t[o]]);
      for (d in 1:D) {
        if (z_rep[d] <= 0)
          y_rep[o, d] = 0;
        else
          y_rep[o, d] = 1;
      }
    }
  }
  for (o in 1:No_h)
      log_lik_h[o] = multi_normal_cholesky_lpdf(z_h[o] | beta*x_h[o], L_Omega[index_cond_h[o]]);

}
