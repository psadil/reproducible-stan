functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  // int<lower=0,upper=1> Correct[ No ]; // The correct choice for a trial
  int<lower=0,upper=1> y[No, D];
  int<lower=0,upper=1> Correct[ No ]; // The correct choice for a trial
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)];
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)];
  int<lower=1,upper=D> d_neg[size(n_neg)];
  N_pos = size(n_pos);
  N_neg = size(n_neg);
  {
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) {
          n_pos[i] = n;
          d_pos[i] = d;
          i = i + 1;
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }
}
parameters{
  matrix[D, K] beta;
  vector[Ns] kappa; // Group-level effects for kappa
  matrix[Ns,D] eta; // Subject-level effects for d' and kappa
  matrix[Ni,D] zeta; // Subject-level effects for d' and kappa
  cholesky_factor_corr[D] L_Omega[K];
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  real<lower=0.0> sigma; // Variance for subject-level effects
  real<lower=0.0> sigma_s; // Variance for subject-level effects
  real<lower=0.0> sigma_i; // Variance for item-level effects
  real<lower=0.0> sigma_k; // Variance for bias

}
transformed parameters {
  vector[D] z[No];

  for (n in 1:N_pos) {
    z[n_pos[n], d_pos[n]] = z_pos[n];
  }
  for (n in 1:N_neg) {
    z[n_neg[n], d_neg[n]] = z_neg[n];
  }
}
model {

  // priors
  for (k in 1:K) {
    L_Omega[k] ~ lkj_corr_cholesky(4.0);
  }
  sigma ~ gamma( 2.0, 4.0 );
  sigma_s ~ gamma( 2.0, 4.0 );
  sigma_i ~ gamma( 2.0, 4.0 ); // mean around .5
  sigma_k ~ gamma( 2.0, 4.0 );

  to_vector(beta) ~ normal(0.0, sigma);
  to_row_vector(eta) ~ normal(0.0, sigma_s);
  to_row_vector(zeta) ~ normal(0.0, sigma_i);
  kappa ~ normal(0.0, sigma_k);

  // likelihood
  // NOTE: define y_i such that if (z_sim_i > 1) {y_i=1}, else {y_i=0}
  // z can be used for posterior retroodictive check
  {
    vector[D] mu;
    for (o in 1:No){
      mu = beta*x[o] +  to_vector(eta[index_s[o]] + zeta[index_i[o]]);
      if (Correct[o]){ // correct is right
        mu[1] = mu[1] + kappa[index_s[o]];
      } else { // correct is left
        mu[1] = -1 * (mu[1] + kappa[index_s[o]]);
      }

      z[o] ~ multi_normal_cholesky(mu, L_Omega[index_cond[o]]);
    }
  }

}
// }
// assemble correlation matrix
generated quantities {
  corr_matrix[D] Omega[K]; // correlation matrix
  vector<lower=0, upper=1>[No] afc;
  vector<lower=0, upper=1>[No] named;

  for (k in 1:K) {
    Omega[k] = multiply_lower_tri_self_transpose(L_Omega[k]);
  }

  for (o in 1:No){
    if (z[o,1] < 0)
      afc[o] = 0;
    else
      afc[o] = 1;
    if (z[o,2] < 0)
      named[o] = 0;
    else
      named[o] = 1;
  }
}
