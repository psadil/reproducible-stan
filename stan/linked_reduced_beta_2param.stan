functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4-1)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject in obs
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item in obs
  int<lower=1,upper=K> index_cond[No]; // Index indicating condition of obs
  int<lower=0,upper=1> y[No, D];
  matrix[ 1 + 3 + 1, 2 ] priors; // lkj + (3*mean of latent) + eta
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)]; // number of 1s, across both questions, given by data
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)]; // number of 0s
  int<lower=1,upper=D> d_neg[size(n_neg)];
  N_pos = size(n_pos);
  N_neg = size(n_neg);
  { // collect index (by trial and decision) of each 1 (pos) and 0 (neg)
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) { // if decision d on trial n was correct
          n_pos[i] = n;   // store that trial number with i
          d_pos[i] = d;   // store that decision with i
          i = i + 1;      // incriment i
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }
}
parameters{
  matrix[D, K] beta; // condition effects on mean of latent
  vector[Ni] zeta[D]; //item effects on mean of latent
  vector[Ns] eta[D]; // Subject effects on mean of latent
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  vector<lower=0.0>[D] sigma; // Variance for subject-level effects
  vector<lower=0.0>[D] sigma_s; // Variance for subject-level effects
  vector<lower=0.0>[D] sigma_i; // Variance for item-level effects
  row_vector<lower=0.0, upper=1.0>[K] mu_cond; //determines mode of correlation from each condition
  row_vector<lower=0>[K] certainty; //certainty on mode for condition effects on correlation
  vector<lower=0.0, upper=1.0>[No] rho_raw;
}
transformed parameters {
  vector[D] z[No];
  cholesky_factor_corr[D] L_Omega[No];
  vector<lower=-1.0, upper=1.0>[No] rho; // actual correlation

  rho = 2.0 * (rho_raw - 0.5);
  { // build cholesky factorized correlation matrix
    matrix[D,D] Omega; // temporary correlation matrix
    for (o in 1:No) {
      Omega = diag_matrix(rep_vector(1,2));
      Omega[1, 2] = rho[o];
      Omega[2, 1] = rho[o];
      L_Omega[o] = cholesky_decompose(Omega);
    }
  }

  // from the indices collected above, create latent variable z. z_pos and z_neg are estimated parameters, pulled from uniform on [-2, 2].
  for (n in 1:N_pos)
    z[n_pos[n], d_pos[n]] = z_pos[n];
  for (n in 1:N_neg)
    z[n_neg[n], d_neg[n]] = z_neg[n];

}

model {

  // components of correlation between decisions
  mu_cond ~ beta(priors[1, 1], priors[1, 2]);
  // mu_s ~ beta(priors[1, 1], priors[1, 2]);
  // mu_i ~ beta(priors[1, 1], priors[1, 2]);
  certainty ~ gamma(priors[2, 1], priors[2, 2]);

  // prior on correlation of decisions
  {
    real mode;
    real k;
    real a;
    real b;
    for(o in 1:No){
      mode = mu_cond * x[o];
      // mode = (mu_cond * x[o] + mu_s[index_s] + mu_i[index_i]) / 3.0; // /3 to normalize back to mode of beta scale
      k = (certainty * x[o]) + 2.0;  // +2: equivalent but less complicated than truncation
      a =  fma(mode, (k - 2.0), 1.0); // mode * (k - 2) + 1;
      b = fma((1.0 - mode), (k - 2.0), 1.0); // (1 - mode) * (k - 2) + 1;
      rho_raw[o] ~ beta(a, b);  // constrained to be between [0, 1]
    }
  }

  // hyper-priors on intercepts and condition effect
  sigma ~ gamma(priors[3, 1], priors[3, 2]);
  sigma_s ~ gamma(priors[4, 1], priors[4, 2]);
  sigma_i ~ gamma(priors[5, 1], priors[5, 2]);

  // priors on intercepts and condition effects
  for (d in 1:D){
    beta[d] ~ normal(0, sigma[d]);
    zeta[d] ~ normal(0, sigma_i[d]);
    eta[d] ~ normal(0, sigma_s[d]);

    // print("target_before = ", target());
    // if (d == 1){
    //   eta[d] ~ gamma(priors[5, 1], priors[5, 2]);
    //   print("eta[1] = ", eta[1]);
    // }
    // else{
    //   eta[d] ~ normal(0, sigma_s);
    //   print("eta[2] = ", eta[2]);
    // }
    // print("target_after = ", target());
  }

  // likelihood
  for (o in 1:No){
    z[o] ~ multi_normal_cholesky(beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_i[o]]), L_Omega[index_cond[o]]);
  }

}
generated quantities {
  int<lower=0,upper=1> y_rep[No, D]; // for posterior retroodictive check

  {
    vector[D] z_rep;
    for (o in 1:No) {
      z_rep = multi_normal_cholesky_rng(beta*x[o] + to_vector(eta[, index_s[o]]) + to_vector(zeta[, index_i[o]]), L_Omega[index_cond[o]]);
      for (d in 1:D) {
        if (z_rep[d] <= 0)
          y_rep[o, d] = 0;
        else
          y_rep[o, d] = 1;
      }
    }
  }
}
