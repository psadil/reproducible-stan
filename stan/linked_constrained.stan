functions {
  int my_sum(int[,] a) {
    int s;
    s = 0;
    for (i in 1:size(a))
    s = s + sum(a[i]);
    return s;
  }
}
data {
  int<lower=1> Ns; // The number of subjects
  int<lower=1> Ni; // The number of items
  int<lower=0, upper=Ns*Ni> No; // number of observations
  int<lower=1> K; // number of conditions (4-1)
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject in obs
  int<lower=1,upper=Ni> index_i[No]; // Index indicating the item in obs
  int<lower=1,upper=K> index_cond[No]; // Index indicating condition of obs
  int<lower=0,upper=1> y[No, D];
}
transformed data {
  int<lower=0> N_pos;
  int<lower=1,upper=No> n_pos[my_sum(y)]; // number of 1s, across both questions, given by data
  int<lower=1,upper=D> d_pos[size(n_pos)];
  int<lower=0> N_neg;
  int<lower=1,upper=No> n_neg[(No * D) - size(n_pos)]; // number of 0s
  int<lower=1,upper=D> d_neg[size(n_neg)];
  N_pos = size(n_pos);
  N_neg = size(n_neg);
  { // collect index (by trial and decision) of each 1 (pos) and 0 (neg)
    int i;
    int j;
    i = 1;
    j = 1;
    for (n in 1:No) {
      for (d in 1:D) {
        if (y[n,d] == 1) { // if decision d on trial n was correct
          n_pos[i] = n;   // store that trial number with i
          d_pos[i] = d;   // store that decision with i
          i = i + 1;      // incriment i
        } else {
          n_neg[j] = n;
          d_neg[j] = d;
          j = j + 1;
        }
      }
    }
  }
}
parameters{
  matrix[D, K] beta;
  matrix[D, Ni] zeta; // Subject-level effects for d' and kappa
  vector[D] eta[Ns]; // Subject-level effects for d' and kappa
  vector<lower=0>[N_pos] z_pos;
  vector<upper=0>[N_neg] z_neg;
  vector<lower=0.0>[D] sigma; // Variance for subject-level effects
  real<lower=0.0> sigma_s; // Variance for subject-level effects
  vector<lower=0.0>[D] sigma_i; // Variance for item-level effects
  row_vector[K] mu_cond; //correlation from each condition
  vector[Ni] mu_i;   ///correlation from each item
  vector[Ns] mu_s;   //correlation from each subject
  real<lower=0> sigma_mu;
  real<lower=0> sigma_mu_s;
  real<lower=0> sigma_mu_i;
  real<lower=0> s_cor;
  vector<lower=-1.0, upper=1.0>[No] rho;
}
transformed parameters {
  vector[D] z[No];
  cholesky_factor_corr[D] L_Omega[No];

  // from the indices collected above, create latent variable z. z_pos and z_neg are estimated parameters, pulled from uniform on [-2, 2].
  for (n in 1:N_pos)
    z[n_pos[n], d_pos[n]] = z_pos[n];
  for (n in 1:N_neg)
    z[n_neg[n], d_neg[n]] = z_neg[n];

  { // build cholesky factorized correlation matrix
    matrix[D,D] Omega; // temporary correlation matrix
    for (o in 1:No) {
      Omega = diag_matrix(rep_vector(1,2));
      Omega[1, 2] = rho[o];
      Omega[2, 1] = rho[o];
      L_Omega[o] = cholesky_decompose(Omega);
    }
  }

}

model {

  // hyper-priors on intercepts and condition corrrelation
  sigma_mu ~ gamma(2, 4);
  sigma_mu_s ~ gamma(2, 4);
  sigma_mu_i ~ gamma(2, 4);
  s_cor ~ gamma(2, 10);  // prior for correlation distribution

  // components of correlation between decisions
  mu_cond ~ normal(0, sigma_mu);
  mu_i ~ normal(0, sigma_mu_i);
  mu_s ~ normal(0, sigma_mu_s);

  // prior on correlation of decisions
  // (vectorization of truncated functions not yet allowed...)
  for(o in 1:No)
    rho[o] ~ normal(mu_cond*x[o] + mu_i[index_i[o]] + mu_s[index_s[o]], s_cor) T[-1.0, 1.0];


  // hyper-priors on intercepts and condition effect
  sigma ~ gamma(2, 4);
  sigma_s ~ gamma(2, 4);
  sigma_i ~ gamma(2, 4);

  // priors on intercepts and condition effects
  for (d in 1:D){
    beta[d] ~ normal(0, sigma[d]);
    zeta[d] ~ normal(0, sigma_i[d]);
    if (d == 1)
      eta[d] ~ gamma(2, 3);
    else
      eta[d] ~ normal(0, sigma_s);
  }

  // likelihood
  for (o in 1:No)
    z[o] ~ multi_normal_cholesky(beta*x[o] + eta[index_s[o]] + zeta[, index_i[o]], L_Omega[o]);

}
