data {
  int<lower=1> Ns; // Number of subjects
  int<lower=1> No; // Number of trials per subject
  int<lower=1> K; // Number of covariates
  int<lower=1> D; // number of outcomes (2)
  vector<lower=0,upper=1>[K] x[No]; // design matrix
  int<lower=1,upper=Ns> index_s[No]; // Index indicating the subject for a current trial
  int<lower=1,upper=K> index_cond[No]; // Index indicating which questions were gotten correct
  matrix[ K+Ns, 2 ] priors; // includes rho
}
model {

}

generated quantities{

  matrix[D, K] beta; // Group-level coefficients for the bivariate normal means
  vector[D] eta[Ns]; // Subject-level coefficients for the bivariate normal means
  vector<lower=0.0>[D] tau; // Variance of subject-level effects
  vector<lower=-1.0,upper=1.0>[No] rho; // Subject-level correlation coefficients
  vector[Ns] s_rho_mu; // subject influence on correlation
  vector[K] cond_rho_mu; // condition influence on correlation
  vector<lower=0.0>[K] rho_sigma; // Group-level standard deviation for correlation

  vector[D] Mu[No]; // Bivariate normal means per trial
  vector[No] rho_mu; // mean for correlation on each trial
  vector[D] z[No];

  // Priors
  for(k in 1:K){
    for(d in 1:D)
    beta[d, k] = normal_rng(0.0, .5); // Mildly informative priors on other parameters
    cond_rho_mu[k] = priors[k, 1];
    rho_sigma[k] = gamma_rng(2.0,6.0);
  }

  for(d in 1:D){
    tau[d] = gamma_rng(2.0,4.0);
  }

  // Hierarchy
  for (s in 1:Ns) {
    s_rho_mu[s] = priors[K+s, 1];
    eta[s, 1] = normal_rng(0.0, tau[1] );
    eta[s, 2] = normal_rng(0.0, tau[2] );
  }

  for (o in 1:No) {
    Mu[o] = beta*x[o] + eta[index_s[o]];
    rho_mu[o] = cond_rho_mu[index_cond[o]] + s_rho_mu[index_s[o]];
  }

  {
    int Ticker;
    real test;
    // matrix[D, D] Sigma; // Covariance matrix for latent variable Z
    //
    // // Fill in fixed values
    // Sigma[1,1] = 1.0;  Sigma[2,2] = 1.0;
    // Sigma[2,1] = 0.0;  Sigma[1,2] = 0.0;
    // print("Sigma = ", Sigma);

    for (o in 1:No) {
      Ticker = 1;
      while (Ticker == 1) {
        test = normal_rng(rho_mu[o], rho_sigma[index_cond[o]]);
        if(test <= -1.0 || test >= 1.0)
          Ticker = 0;
        else
          Ticker = 1;
        // print("test = ", test);
      }
      rho[o] = test;
      // Sigma[1,2] = test; Sigma[2,1] = test;

      // z[o] = multi_normal_rng(Mu[o], Sigma);
    }
  }

{
  matrix[D, D] Sigma; // Covariance matrix for latent variable Z

  // Fill in fixed values
  Sigma[1,1] = 1.0;  Sigma[2,2] = 1.0;
  Sigma[2,1] = 0.0;  Sigma[1,2] = 0.0;
  print("Sigma = ", Sigma);

  // Likelihood
  for(o in1:No){
    Sigma[1,2] = rho[o]; Sigma[2,1] = rho[o];

    // Note that the likelihood is based on the sampled values
    // of the latent variable (i.e. z_pos and z_neg)
    z[o] = multi_normal_rng(Mu[o], Sigma);
  }
}

}
